/**
 * Steuerung der Gartenbeleuchtung
 * 
 * diese soll eingschaltet werden, wenn ...
 * - man es alexa explizit sagt
 * - die tür nach sonnenuntergang geöffnet/offen ist
 *      "nach sonnenuntergang" ist logischerweise vor sonnenaufgang
 */

const gardenDoorId = 'maxcube.0.devices.contact_159468.opened'
const gardenLightId = 'hm-rpc.0.0002170993CBE8.3.STATE'

const switchLightsAtEvening = (val) => {
    if(val) {
        if(!isAstroDay()) {
            setState(gardenLightId, true)
        }
        else {
            setState(gardenLightId, false)
        }
    }
    else {
        setState(gardenLightId, false)
    }
}

// inital state
if(getState(gardenDoorId).val === true) {
    switchLightsAtEvening(true)
}
else {
    switchLightsAtEvening(false)
}

// switch if sun sets and door is open
schedule({astro: 'sunset', shift: -20}, () => {
    if(getState(gartenDoorId).val === true) {
        setState(gardenLightId, true)
    }
})

// switch if door state changes
on(gardenDoorId, (stateObject) => {
    switchLightsAtEvening(stateObject.state.val)
})

