/**
 * Anwesenheitserkennung
 * über Unifi-Adapter
 * 
 * - holt alle 2 Minuten die Daten aus dem Unifi-Status der Geräte, die als 
 *   function="wifidevice" deklariert sind
 * 
 * Einzufügen in den Global-Ordner
 * Kann dann über Presence.all(), Presence.allAway() oder
 * Presence.allAtHome() aufgerufen werden
 */ 

const Presence = (() => {
    
    let devices = []
    const secondsUntilOffline = 180
    
    const _loadData = () => {
        
        const currentTimestamp = Math.floor(Date.now() / 1000);
        
        const wifidevices = $('channel(functions=wifidevice)[state.id=*.hostname]').each((id, name) => {
            const n = getState(id).val;
            const lastSeenId = id.replace('hostname', 'last_seen');
            const lastSeen = getState(lastSeenId).val;
            
            devices[n] = (lastSeen + secondsUntilOffline < currentTimestamp);
        });
    }
    
    return {
        init: () => {
            _loadData()
            schedule('*/2 * * * *', _loadData);
            
            return this;
        },
        all: () => {
            return devices;
        },
        allAway: () => {
            for(var name in devices) {
                if(devices[name] === true) {
                    return false;
                }
            }
            return true;
        },
        allAtHome: () => {
            for(var name in devices) {
                if(devices[name] === false) {
                    return false;
                }
            }
            return true;
        }
    }
})();

Presence.init();

