/**
 * Steuerung der Lüftung im Waschkeller
 * 
 * Lüfter soll angehen, wenn
 * - Luftfeuchtigkeit höher als 65% ist
 * 
 * Lüfter soll ausgehen,
 * - Luftfeuchtigkeit niedriger als 65% ist
 */
 
const basementFanId = 'hm-rpc.0.0002170993CCE2.3.STATE'
const basementHumidityId = 'netatmo.0.Danziger-Straße.Keller.Humidity.Humidity'

const controlBasementFan = (currentHumidity) => {
    if(currentHumidity > 70) {
        setState(basementFanId, true)
    }
    else {
        setState(basementFanId, false)
    }
}

// update on humidity changes
on(basementHumidityId, function(stateObject) {
    controlBasementFan(stateObject.state.val)
})

// initial state
controlBasementFan(
    getState(basementHumidityId).val
)
